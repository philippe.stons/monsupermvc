﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using MonSuperMVC.Models;

namespace MonSuperMVC.Infrastructure.Validations
{
    public class ContactIdValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int contactID = (int)value;

            DataContext data = new DataContext();

            Contact contact = (from c in data.Contacts
                              where c.ID == contactID
                              select c).SingleOrDefault();

            if (contact is null) 
            {
                return new ValidationResult("Ce contact n'existe pas!");
            }

            return ValidationResult.Success;
        }
    }
}
