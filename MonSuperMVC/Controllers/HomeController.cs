﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MonSuperMVC.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MonSuperMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Exemple() 
        {
            return View();
        }
        // www.aldaksld.com/home/utilisateur?firstname=asdf&&lastname=asdasd&&username=adaq
        public IActionResult Utilisateur(string firstname, string lastname, string username) 
        {
            ViewBag.Username = "riri";
            ViewData["Message"] = "Hello world!";
            ViewBag.FirstName = "Philippe";
            ViewData["LastName"] = "Stons";

            User user = new User()
            {
                FirstName = firstname,
                LastName = lastname,
                Username = username
            };

            return View(user);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
