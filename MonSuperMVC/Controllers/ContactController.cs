﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MonSuperMVC.Models.Form;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MonSuperMVC.Controllers
{
    public class ContactController : Controller
    {
        private DataContext data;

        public ContactController(DataContext data) 
        {
            this.data = data;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List() 
        {
            var contacts = from c in data.Contacts select c;
            return View(contacts);
        }

        public IActionResult Details(int id) 
        {
            var contact = (from c in data.Contacts where c.ID == id select c).FirstOrDefault();
            return View(contact);
        }
        // contact/contactrdv/5
        public IActionResult ContactRdv(int id) 
        {
            RdvBox rdvs = new RdvBox();
            rdvs.Dates = from r in data.RDVs
                          where r.ContactId == id
                          select new SelectListItem(r.Date.ToShortDateString(), 
                          r.RdvId.ToString());

            return View(rdvs);
        }

        public IActionResult Create() 
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create([FromForm] CreateContactForm form) 
        {
            if (!ModelState.IsValid) 
            {
                return View(form);
            }
            Console.WriteLine($"New contact : {form.Nom} {form.Prenom} {form.Email} {form.AnneeDeNaissance}");

            data.Contacts.Add(
                    new Models.Contact() 
                    {
                        Nom = form.Nom,
                        Prenom = form.Prenom,
                        Email = form.Email,
                        AnneeDeNaissance = form.AnneeDeNaissance,
                        ID = data.Contacts.Count + 1
                    }
                );

            return RedirectToAction("List");
        }
    }
}
