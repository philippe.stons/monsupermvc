﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MonSuperMVC.Models.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonSuperMVC.Controllers
{
    public class UserController : Controller
    {
        private DataContext data;

        public UserController(DataContext data) 
        {
            this.data = data;
        }
        // GET: UserController
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        // GET: UserController/Details/5
        public ActionResult Details(int id)
        {
            var user = (from u in data.Users
                       where u.UserId == id
                       select new DetailUserForm(u))
                       .FirstOrDefault();
            return View(user);
        }

        public IActionResult List() 
        {
            var users = from u in data.Users
                        select new DetailUserForm(u);
            return View(users);
        }

        // GET: UserController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: UserController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: UserController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
