﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MonSuperMVC.Models.Form;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MonSuperMVC.Controllers
{
    public class RdvController : Controller
    {
        private DataContext data;

        public RdvController(DataContext data) 
        {
            this.data = data;
        }

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List() 
        {
            var rdvs = from r in data.RDVs
                       select r;

            return View(rdvs);
        }

        public IActionResult Details(int id) 
        {
            var rdv = (from r in data.RDVs
                      where r.RdvId == id
                       select r).FirstOrDefault();

            return View(rdv);
        }

        public IActionResult Create() 
        {
            RdvForm rdv = new RdvForm();
            rdv.Emails = from c in data.Contacts
                         select new SelectListItem(c.Email, c.ID.ToString());
            return View(rdv);
        }

        [HttpPost]
        public IActionResult Create([FromForm] RdvForm form) 
        {
            if (!ModelState.IsValid)
            {
                form.Emails = from c in data.Contacts
                             select new SelectListItem(c.Email, c.ID.ToString());
                return View(form);
            }
            Console.WriteLine($"New rdv : {form.ContactId} {form.Date.ToShortDateString()}");

            data.RDVs.Add(new Models.RDV()
            {
                RdvId = data.RDVs.Count + 1,
                ContactId = form.ContactId,
                Date = form.Date,
                Email = (from c in data.Contacts
                         where c.ID == form.ContactId
                         select c).FirstOrDefault().Email
            }); ;
            return RedirectToAction("List");
        }
    }
}
