﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using MonSuperMVC.Infrastructure.Validations;

namespace MonSuperMVC.Models.Form
{
    public class RdvForm
    {
        [Required]
        [ContactIdValidation]
        [DisplayName("Email : ")]
        public int ContactId { get; set; }

        public IEnumerable<SelectListItem> Emails { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Date { get; set; }
    }
}
