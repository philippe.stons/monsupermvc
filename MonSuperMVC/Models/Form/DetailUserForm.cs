﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonSuperMVC.Models.Form
{
    public class DetailUserForm
    {
        public DetailUserForm(User u) 
        {
            UserId = u.UserId;
            FirstName = u.FirstName;
            LastName = u.LastName;
            Username = u.Username;
            role = u.role;
        }

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public Role role { get; set; }
    }
}
