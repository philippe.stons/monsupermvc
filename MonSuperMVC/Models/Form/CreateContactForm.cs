﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MonSuperMVC.Models.Form
{
    public class CreateContactForm
    {
        [Required]
        [StringLength(75)]
        [DisplayName("Nom : ")]
        public string Nom { get; set; }
        [Required]
        [StringLength(75)]
        [DisplayName("Prenom : ")]
        public string Prenom { get; set; }
        [Required]
        [StringLength(75)]
        [EmailAddress]
        [DisplayName("Email : ")]
        public string Email { get; set; }
        [Required]
        [Range(1950, 2150)]
        [DisplayName("Annee de naissance : ")]
        public int AnneeDeNaissance { get; set; }
    }
}
