﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using MonSuperMVC.Infrastructure.Validations;

namespace MonSuperMVC.Models.Form
{
    public class RdvBox
    {
        [Required]
        [DisplayName("Dates : ")]
        public int RdvId { get; set; }

        public IEnumerable<SelectListItem> Dates { get; set; }
    }
}
