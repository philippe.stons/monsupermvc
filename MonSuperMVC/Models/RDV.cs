﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonSuperMVC.Models
{
    public class RDV
    {
        public int RdvId { get; set; }
        public int ContactId { get; set; }
        public string Email { get; set; }
        public DateTime Date { get; set; }
    }
}
