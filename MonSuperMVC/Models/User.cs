﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonSuperMVC.Models
{
    public enum Role 
    {
        User,
        Admin,
        Guest
    }
    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string password { get; set; }
        public Role role { get; set; }
    }
}
