﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using MonSuperMVC.Models;

namespace MonSuperMVC
{
    public class DataContext
    {
        public List<Contact> Contacts;
        public List<RDV> RDVs;
        public List<User> Users;

        public DataContext()
        {
            this.Contacts = new List<Contact>();

            this.Contacts.AddRange(new Contact[] {
                    new Contact() { ID = 1,  Nom = "Person", Prenom = "Michael", Email = "michael.person@cognitic.be", AnneeDeNaissance = 1982 },
                    new Contact() { ID = 2,  Nom = "Morre", Prenom = "Thierry", Email = "thierry.morre@cognitic.be", AnneeDeNaissance = 1974 },
                    new Contact() { ID = 3,  Nom = "Dupuis", Prenom = "Thierry", Email = "thierry.dupuis@cognitic.be", AnneeDeNaissance = 1988 },
                    new Contact() { ID = 4,  Nom = "Faulkner", Prenom = "Stephane", Email = "stephane.faulkner@cognitic.be", AnneeDeNaissance = 1969 },
                    new Contact() { ID = 5,  Nom = "Selleck", Prenom = "Tom", Email = "tom.selleck@email.com", AnneeDeNaissance = 1945 },
                    new Contact() { ID = 6,  Nom = "Anderson", Prenom = "Richard Dean", Email = "richard.dean.anderson@email.com", AnneeDeNaissance = 1950 },
                    new Contact() { ID = 7,  Nom = "Bullock", Prenom = "Sandra", Email = "sandra.bullock@hotmail.com", AnneeDeNaissance = 1964 },
                    new Contact() { ID = 8,  Nom = "Peppard", Prenom = "George", Email = "george.peppard@hotmail.com", AnneeDeNaissance = 1928 },
                    new Contact() { ID = 9,  Nom = "Estevez", Prenom = "Emilio", Email = "emilio.estevez@hotmail.com", AnneeDeNaissance = 1962 },
                    new Contact() { ID = 10,  Nom = "Moore", Prenom = "Demi", Email = "demi.moore@gmail.com", AnneeDeNaissance = 1962 },
                    new Contact() { ID = 11,  Nom = "Willis", Prenom = "Bruce", Email = "bruce.willis@gmail.com", AnneeDeNaissance = 1955 }
                });

            this.RDVs = new List<RDV>();

            this.RDVs.AddRange(
                    new RDV[]
                    {
                        new RDV() { RdvId = 1, ContactId = 4, Email = "stephane.faulkner@cognitic.be", Date = new DateTime(2012, 5, 12) },
                        new RDV() { RdvId = 2, ContactId = 8, Email = "george.peppard@hotmail.com", Date = new DateTime(2011, 8, 14) },
                        new RDV() { RdvId = 3, ContactId = 11, Email = "bruce.willis@gmail.com", Date = new DateTime(2012, 6, 19) },
                        new RDV() { RdvId = 4, ContactId = 11, Email = "bruce.willis@gmail.com", Date = new DateTime(2012, 6, 20) },
                        new RDV() { RdvId = 5, ContactId = 1, Email = "michael.person@cognitic.be", Date = new DateTime(2012, 4, 19) },
                    }
                );

            this.Users = new List<User>();

            this.Users.AddRange(
                    new User[] 
                    {
                        new User() { UserId = 1, FirstName = "Richard", LastName = "Wagner", password = "WWV63", Username = "rwagner", role = Role.User },
                        new User() { UserId = 2, FirstName = "Johann Christian", LastName = "Bach", password = "WWV63", Username = "jcbach", role = Role.User },
                        new User() { UserId = 3, FirstName = "Johann Sebastian", LastName = "Bach", password = "BWV1001", Username = "jsbach", role = Role.Admin },
                        new User() { UserId = 4, FirstName = "Anton", LastName = "Bruckner", password = "WWV63", Username = "abruckner", role = Role.User },
                        new User() { UserId = 5, FirstName = "Johann", LastName = "Brahms", password = "WWV63", Username = "jbrahms", role = Role.User },
                    }
                );
        }
    }
}
